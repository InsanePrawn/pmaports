# Maintainer: Miles Alan <m@milesalan.com>
pkgname=sxmo-dmenu
pkgver=5.0.8
pkgrel=0
pkgdesc="Dmenu fork for Sxmo UI; supports highlight, centering, volume-key navigation and more"
url="https://git.sr.ht/~mil/sxmo-dmenu"
arch="all"
license="MIT"
makedepends="libx11-dev libxinerama-dev libxft-dev"
options="!check" # has no tests
subpackages="$pkgname-doc"
provides="dmenu"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~mil/sxmo-dmenu/archive/$pkgver.tar.gz"

prepare() {
	default_prepare
	sed -i -e '/CFLAGS/{s/-Os//;s/=/+=/}' \
		-e '/LDFLAGS/{s/=/+=/}' \
		"$builddir"/config.mk
}

build() {
	make X11INC=/usr/include/X11 \
		X11LIB=/usr/lib/X11 \
		FREETYPEINC=/usr/include/freetype2 \
		-C "$builddir"
}

package() {
	make DESTDIR=$pkgdir PREFIX=/usr \
		-C "$builddir" install
}

sha512sums="2e03fe747c5e227f402d8acec5bdcfadc0f382204bfc382035d5dbd0a6fbf0136350c3cf72be397b52965c1b75c14c16e20ab69da092baaacc41764ce5ccc846  sxmo-dmenu-5.0.8.tar.gz"
